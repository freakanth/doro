#!/bin/bash

# D'oro: A minimal command-line Pomodoro Timer application
# Copyright (C) 2017 Srikanth Cherla
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DORO_HOME="$HOME/.doro"
TOMATO_PATH=$1/tomato.png  

# Start work timer
date > $DORO_HOME/start_time

notify-send -u normal -t 5000 -i $TOMATO_PATH "D'oro Says:" \
	"Started timer for 50 minutes."
sleep 3000 && notify-send -u normal -t 5000 -i $TOMATO_PATH "D'oro Says:" \
	"Take a 10 minute break.\nIt's good for you."

# Start break timer
date > $DORO_HOME/start_time
sleep 600 && notify-send -u normal -t 5000 -i $TOMATO_PATH "d'Oro Says:" \
	"Hope that break did you good.\nIt's time to get back to work now!"

rm $DORO_HOME/start_time
