# D'oro: A Minimal Command-line Pomodoro Timer Application

The [Pomodoro Technique](https://en.wikipedia.org/wiki/Pomodoro_Technique) is a 
time management method developed by Francesco Cirillo in the late 1980s. The 
technique uses a timer to break down work into intervals, traditionally 25 
minutes in length, separated by short breaks. These intervals are named 
pomodoros, the plural in English of the Italian word pomodoro (tomato), after 
the tomato-shaped kitchen timer that Cirillo used as a university student
(source: Wikipedia).

## Requirements
D'oro has minimal dependencies. It is written in BASH and uses commands that
are available by default in most Linux distributions. In order for the desktop 
notifications to work, the command `notify-send` is required. In Debian/Ubuntu
Linux, it can be installed via `apt-get`

```
sudo apt-get install ruby-notify
```

## Installation
If your username is `bob` and you clone the repository into your home folder
`/home/bob`, then all you need to do in order to install D'oro is to create a
symbolic link to the executable file named `doro` inside the repository as
follows:

```
sudo ln -s /home/bob/doro/doro /usr/local/bin/doro
```

Following this, you will be able to call D'oro from the terminal (or even your
program launcher) using the command `doro`.

## Usage
You can start a new timer, stop a running timer or check whether or not a
timer. 

To start a timer, type

```
doro start
```

This will activate a new timer, will pop up a notification after 25 minutes to 
take a break and automatically begin a break timer, and finally exit the 
program at the end of the break timer.

To start a timer that loops continuously between work and break sessions, type

```
doro forever
```

This will activate a new timer, followed by a break timer in the same manner as
`doro start` but will instead loop back to the start of the work timer at the
end of the break timer instead of exiting the program.

To stop a running timer, type

```
doro stop
```

This stops an active timer, or do nothing if there isn't one.

To check whether 

```
doro check
```

This will tell you when the running timer was activated, and do nothing if
there isn't one.
