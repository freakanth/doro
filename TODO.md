* Have multiple messages that are picked at random in the notification.
* Display time until the end of the running timer in `doro check`.
* Write an installation script.
* Create a Debian package.
